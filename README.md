**This is the Assignment No: 1 for the Distributed Computing - BITS 3rd Semester** 
**Part 1**

## Prerequisites

 The following software should be installed on your machine in order to get it working
 
 1. gcc
 2. rpcgen
 3. vi
 

## How to compile?


1. Open the terminal and enter the root directory of the project.
2. make clean && make


This will generate the binaries in the bin folder
1. RpcServer
2. RpcClient

---

## How to run?

Run the server from the terminal

1. Locate the bin and enter into it 
2. ./RpcServer

This will run the rpc server in the background. In order to kill the server from the terminal,  

1. ps -e | grep 'RpcServer'   // this will show you the PID of the RpcServer process. Copy it
2. kill -9 'pid of the RpcServer'


## How to run client program

Run client program from the terminal

1. Locate the bin and enter into it
2. ./RpcClient localhost   (if we are running the server on the same machine)

And continue.


## Change the Specification

After changing the specification (src/common/calculator.x), you need to do the following stpes.

1. make gencode    (This will generate both client and server stubs
2. make clean
3. make

Then change the src/client/client.c and src/server/server.c according to your specification chages.


**Part2**

LamportLogicalClock folder contains LamportLogicalClock.java file which implements the part 2 of the assignment. 
This is done is a generic way such that it can accept arbitrary number of processes and each process can have 
any number of events. User can enter the total number of sendMessage events followed by each sendMessage event
detaials such as sendProcessId, sendEventNum, recvProcessId and recvEvtNum. This will calculate the Lamport
logical clock of each process as the result. No third part library has been used, you can compile the java file 
using "javac".

In order to compile and run

1. Get into the folder LamportLogicalClock
2. javac LamportLogicalClock.java
3. java LamportLogicalClock



