#include "../common/calculator.h"

void calc_prg_1(char *host) {
	CLIENT *clnt;
	output *result_1;
	input calc_1_arg;
  calc_1_arg.choice = 0;
	clnt = clnt_create (host, calc_prg, calc_version, "udp");
	if (clnt == NULL) {
  		clnt_pcreateerror(host);
		exit (1);
 	}
  do  {
    printf("Please select a choice from 1 to 7 as shown below");
   	printf("\n1 Addition \n2.Multiplication \n3.Subtraction \n4.Division \n5.Remainder \n6.Is first number prime\n7.Check Odd or Even\nEnter choice (1 to 7): ");
  	if (!scanf("%d",&calc_1_arg.choice)) {
      calc_1_arg.choice = 0;
    }
  } while((calc_1_arg.choice <= 0 || calc_1_arg.choice >= 8));
	printf("Enter values: ");
	if (!scanf("%d %d",&calc_1_arg.x,&calc_1_arg.y)) {
    printf("---------------------------------------------------\n");
    printf("Error: Invalid arguments for the selected operation\n");
    printf("---------------------------------------------------\n");
    exit(1);
  }
 	result_1 = calc_1(&calc_1_arg, clnt);
 	if (result_1 == (output *) NULL) {
 		clnt_perror (clnt, "Call failed");
 	}
	if(calc_1_arg.choice != 6 && calc_1_arg.choice != 7) {
    if (result_1->isSuccess) {
      printf("---------------\n");
      printf("Result %d \n", result_1->result);
      printf("---------------\n");
    } else {
      printf("---------------------------------------------------\n");
      printf("Error: Invalid arguments for the selected operation\n");
      printf("---------------------------------------------------\n");
    }
  } else {
    if(calc_1_arg.choice==6){
      if (result_1->result==1) {
          printf("-----------------\n");
        	printf("Prime Number: YES\n");
          printf("-----------------\n");
      } else {
        printf("----------------\n");
        printf("Prime Number: No\n");
        printf("----------------\n");
      }
    } else if (calc_1_arg.choice==7){
      if (result_1->oddOrEven == 0) {
        printf("---------------------------\n");
        printf("Both inputs are odd numbers\n");
        printf("---------------------------\n");
      } else if (result_1->oddOrEven == 1) {
        printf("----------------------------\n");
        printf("Both inputs are even numbers \n");
        printf("----------------------------\n");
      } else {
        printf("-----------------------------------\n");
        printf("Inputs has both odd and even number\n");
        printf("-----------------------------------\n");
      }
    }
  }
	clnt_destroy (clnt);
}
int main (int argc, char *argv[]) {
	char *host;
	if (argc < 2) {
 		printf ("usage: %s server_host\n", argv[0]);
 		exit (1);
 	}
 	host = argv[1];
 	calc_prg_1 (host);
	exit (0);
}
