/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "../common/calculator.h"

/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25, 0 };

output *
calc_1(argp, clnt)
	input *argp;
	CLIENT *clnt;
{
	static output clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, calc, xdr_input, argp, xdr_output, &clnt_res, TIMEOUT) != RPC_SUCCESS)
		return (NULL);
	return (&clnt_res);
}
