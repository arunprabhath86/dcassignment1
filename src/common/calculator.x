struct input {
	int x;
	int y;
	int ans;
	int choice;
};

struct output {
	int result;
	bool isSuccess;
	int oddOrEven;
};
program calc_prg {
 	version calc_version {
 		output calc(input)=1;
	}=1;
}=0x32345676;
