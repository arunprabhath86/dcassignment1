CC=gcc
LIBS=-lpthread -lstdc++ -lrpcsvc
CFLAGS=-w
SERVER_CFLAGS=-DRPC_SVC_FG
SOURCES_COMMON=$(wildcard src/common/*.c)
SOURCES_SERVER=$(wildcard src/server/*.c)
SOURCES_CLIENT=$(wildcard src/client/*.c)
OBJECTS_COMMON=$(patsubst %.c, %.o, $(SOURCES_COMMON))
OBJECTS_SERVER=$(patsubst %.c, %.o, $(SOURCES_SERVER))
OBJECTS_CLIENT=$(patsubst %.c, %.o, $(SOURCES_CLIENT))
EXECUTABLE_SERVER=bin/RpcServer
EXECUTABLE_CLIENT=bin/RpcClient

all: build $(EXECUTABLE_SERVER) $(EXECUTABLE_CLIENT)

$(EXECUTABLE_SERVER):  $(OBJECTS_SERVER) $(OBJECTS_COMMON)
	$(CC) $(CFLAGS) $(OBJECTS_SERVER) $(OBJECTS_COMMON) $(LIBS) -o $@ $(SERVER_CFLAGS)

$(EXECUTABLE_CLIENT):  $(OBJECTS_CLIENT) $(OBJECTS_COMMON)
	$(CC) $(CFLAGS) $(OBJECTS_CLIENT) $(OBJECTS_COMMON) $(LIBS) -o $@

$(OBJECTS_SERVER): src/server/%.o : src/server/%.c
	$(CC) $(CFLAGS) -c $< $(LIBS) -o $@

$(OBJECTS_CLIENT): src/client/%.o : src/client/%.c
	$(CC) $(CFLAGS) -c $< $(LIBS) -o $@

$(OBJECTS_COMMON): src/common/%.o : src/common/%.c
	$(CC) $(CFLAGS) -c $< $(LIBS) -o $@

build:
	mkdir -p bin

gencode:
	rpcgen ./src/common/calculator.x
	#rpcgen -C -Ss ./src/common/calculator.x > ./src/server/server.c  ## Don't uncomment this line, this would rewrite the exisitng server.c
	mv calculator.h ./src/common/
	mv calculator_xdr.c ./src/common/
	mv calculator_svc.c ./src/server/
	mv calculator_clnt.c ./src/client/
	#vi ./src/server/server.c -c '%s/calculator\.h/..\/common\/calculator.h/g' -c ':wq' ## This line is not needed, if you are not generating server side code using rpcgen.
	vi ./src/server/calculator_svc.c -c '%s/calculator\.h/..\/common\/calculator.h/g' -c ':wq'
	vi ./src/client/calculator_clnt.c -c '%s/calculator\.h/..\/common\/calculator.h/g' -c ':wq'

clean:
	rm -rf $(EXECUTABLE_SERVER) $(EXECUTABLE_CLIENT) $(OBJECTS_COMMON) $(OBJECTS_SERVER) $(OBJECTS_CLIENT) bin
