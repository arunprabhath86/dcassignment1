import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class LamportLogicalClock {


	private static Scanner scanner = new Scanner(System.in);

	private int nProcesses;

	private int d = 1;

	private int startClock = 1;

	private HashMap<Integer, ArrayList<Integer>> processes = new HashMap<Integer, ArrayList<Integer>>();

	private ArrayList<MessageEvent> messageEvents = new ArrayList<MessageEvent>();



	public void start() {

		System.out.println("LamportLogiclClock Simulator");
		System.out.print("Enter total number of processes : ");
		nProcesses = scanner.nextInt();
		System.out.print("Enter the value of d (Default = 1): ");
		d = scanner.nextInt();
		System.out.print("Enter the start value of clock (common for all processes, default = 1) : ");
		startClock = scanner.nextInt();
		initialize();
		readSendAndReceiveEvents();
		processLamportLogiclClock();
		printResult();
	}


	private void printResult() {
		System.out.println("-------------------------Result----------------------------");
		for (int eachPid = 1; eachPid <= nProcesses; eachPid++) {
			System.out.println("Time Stamp value for Process("+ eachPid + "): " + processes.get(eachPid));
		}
	}

	private MessageEvent isRecievingEvent(int recvPid, int recvEventId) {
		for (int i = 0; i< messageEvents.size(); i++) {
			MessageEvent event = messageEvents.get(i);
			if (event.getRecvProcessNum() == recvPid && event.getRecvEventNum() == recvEventId) {
				return event;
			}
		}
		return null;
	}


	private void processLamportLogiclClock() {
		for (int eachPid = 1; eachPid <= nProcesses; eachPid++) {
			ArrayList<Integer> processEvents = processes.get(eachPid);
			for (int eachEvnId = 0; eachEvnId< processEvents.size(); eachEvnId++) {
				MessageEvent messageEvent = isRecievingEvent(eachPid, eachEvnId + 1);
				if ( messageEvent != null) {
					int sendProcId = messageEvent.getFromProcessNum();
					int sendEventId = messageEvent.getFromEventNum();
					processEvents.set(eachEvnId,
							(Math.max(processes.get(sendProcId).get(sendEventId -1),
									processEvents.get(eachEvnId)) + d));

				} else {
					if (eachEvnId != 0) {
						processEvents.set(eachEvnId,
								processEvents.get(eachEvnId - 1) + d);
					}
				}
			}
		}
	}

	private void readSendAndReceiveEvents() {
		System.out.print("Enter the totalnumber of send message events: ");
		int sendEvents = scanner.nextInt();
		for (int i=1; i <= sendEvents; i++) {
			System.out.print("\n\nEnter the Message details of Message event(" + i + ")\n");
			System.out.print("Enter the Sending Process Id (1 to " + nProcesses + "): ");
			int fromProcess = scanner.nextInt();
			System.out.print("Enter the event num which sends the message from the Process(" + fromProcess + "): ");
			int fromEvent = scanner.nextInt();
			System.out.print("Enter the Receiving Process Id (1 to " + nProcesses + "): ");
			int recvProcess = scanner.nextInt();
			System.out.print("Enter the event num which receives the message at the Process(" + recvProcess + "): ");
			int recvEvent = scanner.nextInt();
			MessageEvent messageEvent = new MessageEvent(fromProcess, fromEvent, recvProcess, recvEvent);
			messageEvents.add(messageEvent);
		}
	}


	private void initialize() {
		for (int i = 1; i <= nProcesses; i++) {
			System.out.print("Enter the total number of events in Process " + i + ":");
			int nEvents = scanner.nextInt();
			ArrayList<Integer> events = new ArrayList<Integer>();
			for (int j=0; j< nEvents; j++) {
				if (j ==0) {
					events.add(startClock);
				} else {
					events.add(events.get(j-1) + d);
				}
			}
			processes.put(i, events);
		}
	}


	public static void main(String...a) {

		LamportLogicalClock lamportLogiclClock = new LamportLogicalClock();
		lamportLogiclClock.start();
	}

}

class MessageEvent {

	int fromProcessNum;
	int fromEventNum;
	int recvProcessNum;
	int recvEventNum;

	public MessageEvent() {

	}

	public MessageEvent(int frmProc, int fromEvnt, int rcvProc, int rcvEvnt) {
		this.fromProcessNum = frmProc;
		this.fromEventNum = fromEvnt;
		this.recvProcessNum = rcvProc;
		this.recvEventNum = rcvEvnt;
	}

	public int getFromProcessNum() {
		return fromProcessNum;
	}
	public void setFromProcessNum(int fromProcessNum) {
		this.fromProcessNum = fromProcessNum;
	}
	public int getFromEventNum() {
		return fromEventNum;
	}
	public void setFromEventNum(int fromEventNum) {
		this.fromEventNum = fromEventNum;
	}
	public int getRecvProcessNum() {
		return recvProcessNum;
	}
	public void setRecvProcessNum(int recvProcessNum) {
		this.recvProcessNum = recvProcessNum;
	}
	public int getRecvEventNum() {
		return recvEventNum;
	}
	public void setRecvEventNum(int recvEventNum) {
		this.recvEventNum = recvEventNum;
	}

	public String toString() {
		return "[fromProcessNum: "+ this.fromProcessNum+", fromEventNum:"
				+ this.fromEventNum + ", recvProcessNum: "
				+ this.recvProcessNum + ", recvEventNum: "+ this.recvEventNum + "]";
	}

}
