
--------------------------------------------------------------------------------------------------------------------------------
All source code are checked in to my personal bitbucket location: https://bitbucket.org/arunprabhath86/dcassignment1/src/master/
In the above repository, all instructions are updated in README.md file.
--------------------------------------------------------------------------------------------------------------------------------


This is the Assignment No: 1 for the Distributed Computing - BITS 3rd Semester Part 1

Prerequisites
The following software should be installed on your machine in order to get it working

* gcc
* rpcgen
* vi

How to compile?

Open the terminal and enter the root directory of the project.

	make clean && make

This will generate the binaries in the bin folder 1. RpcServer 2. RpcClient

How to run?

Run the server from the terminal

Locate the bin and enter into it
./RpcServer
This will run the rpc server in the background. In order to kill the server from the terminal,

ps -e | grep 'RpcServer' // this will show you the PID of the RpcServer process. Copy it
kill -9 'pid of the RpcServer'

How to run client program
Run client program from the terminal

Locate the bin and enter into it
./RpcClient localhost (if we are running the server on the same machine)
And continue.

Change the Specification
After changing the specification (src/common/calculator.x), you need to do the following stpes.

	make gencode (This will generate both client and server stubs
	make clean
	make

Then change the src/client/client.c and src/server/server.c according to your specification chages.




Part2

LamportLogicalClock folder contains LamportLogicalClock.java file which implements the part 2 of the assignment. This is done is a generic way such that it can accept arbitrary number of processes and each process can have any number of events. User can enter the total number of sendMessage events followed by each sendMessage event detaials such as sendProcessId, sendEventNum, recvProcessId and recvEvtNum. This will calculate the Lamport logical clock of each process as the result. No third part library has been used, you can compile the java file using "javac".

In order to compile and run

Get into the folder LamportLogicalClock
javac LamportLogicalClock.java
java LamportLogicalClock


Once the part 1 and Part 2 are compiled, the following tree structure will be the folder structure of the application.
-----------------------------
The project folder structure
-----------------------------

.
├── LamportLogicalClock
│   ├── LamportLogicalClock.class
│   ├── LamportLogicalClock.java
│   ├── MessageEvent.class
│   └── screenshot
│       ├── LamportExample1.png
│       ├── LamportExample2.png
│       └── LamportExample3.png
├── Makefile
├── README.md
├── bin
│   ├── RpcClient
│   └── RpcServer
└── src
    ├── client
    │   ├── calculator_clnt.c
    │   ├── calculator_clnt.o
    │   ├── client.c
    │   └── client.o
    ├── common
    │   ├── calculator.h
    │   ├── calculator.x
    │   ├── calculator_xdr.c
    │   └── calculator_xdr.o
    ├── screenshots
    │   ├── RPC_run1.png
    │   ├── RPC_run2.png
    │   ├── RPC_run3.png
    │   ├── RPC_run4.png
    │   ├── RPC_run5.png
    │   └── RPC_run6.png
    └── server
        ├── calculator_svc.c
        ├── calculator_svc.o
        ├── server.c
        └── server.o

All source code are checked in to my personal bitbucket location: https://bitbucket.org/arunprabhath86/dcassignment1/src/master/



